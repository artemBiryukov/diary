package application.api;

import application.event.Event;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

@RestController
public class Controller {
    @Autowired
    private MongoTemplate template;
    private GsonBuilder gb = new GsonBuilder();
    private Gson gson = gb.create();

    @RequestMapping(value = "/addEvent")
    private void addEvent(@RequestBody String s) {
        try {
            String result = java.net.URLDecoder.decode(s, StandardCharsets.UTF_8.name());
            Event event = gson.fromJson(result, Event.class);
            template.insert(event, "events");
            System.out.println(result);
        } catch (UnsupportedEncodingException e) {

        }
    }

    @RequestMapping(value = "/getEvents", params = {"date"})
    private List<Event> getEventsByDate(@RequestParam(value = "date") String date) {
        Query query = new Query().addCriteria(Criteria.where("date").is(date));
        List<Event> list = template.find(query, Event.class);
        if (list.isEmpty()) {
            return Collections.singletonList(new Event("", ""));
        }
        return list;
    }

    @RequestMapping(value = "/deleteEventById", params = "id")
    private String deleteEvent(@RequestParam(value = "id") String id) {
        Query query = new Query().addCriteria(Criteria.where("id").is(id));
        Event event = template.findAndRemove(query, Event.class);
        if (event != null) {
            return "deleted event " + event.getId();
        } else {
            return "event doesn't exist.";
        }
    }
}
